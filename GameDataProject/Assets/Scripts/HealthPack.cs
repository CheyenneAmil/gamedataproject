﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour {


    public Vector3 destination;
    public Vector3 origPos;

    public float timePassed;
    public float lerpTime;
    public float t;

    public bool positiveDir;


    private void Awake()
    {
        origPos = this.transform.position;
        destination = new Vector3(Random.Range(-1, 2), Random.Range(-1, 2), 0);
        t = 0;
        
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (destination == new Vector3(0, 0, 0))
        {
            destination = new Vector3(Random.Range(-1, 2), Random.Range(-1, 2), 0);
        }

        timePassed += Time.deltaTime;  
        if (timePassed < lerpTime  && positiveDir )
        {
            t = (timePassed *0.1f)/ lerpTime;
            this.transform.position = Vector3.Lerp(this.transform.position, origPos + destination, t);

        }
        if( timePassed >=lerpTime && positiveDir)
        {
            positiveDir = false;
            timePassed = 0;
        }



        if (timePassed < lerpTime && !positiveDir )
        {
            t = (timePassed * 0.1f) / lerpTime;
            this.transform.position = Vector3.Lerp(this.transform.position, origPos, t);

        }
        if (timePassed >= lerpTime && !positiveDir)
        {
            positiveDir = true;
            destination = new Vector3(Random.Range(-1, 2), Random.Range(-1, 2), 0);
            timePassed = 0;
        }






    }
}
