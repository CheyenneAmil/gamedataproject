﻿using System;
using UnityEngine;

[Serializable]
public class HealthSave : Save {

    public Data data;
    private HealthPack healthPack;
    private string jsonString;


    [Serializable]
    public class Data : BaseData
    {
        public Vector3 position;
        public Vector3 originalPosition; 
        public Vector3 eulerAngles;
        public Vector3 destination;
        public float timePassed;

    }

    void Awake()
    {
        healthPack = GetComponent<HealthPack>();
        data = new Data();
    }




    public override string Serialize()
    {
        data.prefabName = prefabName;
        data.position = healthPack.transform.position;
        data.originalPosition = healthPack.origPos;
        data.eulerAngles = healthPack.transform.eulerAngles;
        data.destination = healthPack.destination;
        data.timePassed = healthPack.timePassed; 

        jsonString = JsonUtility.ToJson(data);


        return (jsonString);
    }



    public override void Deserialize(string jsonData)
    {
        JsonUtility.FromJsonOverwrite(jsonData, data);

            healthPack.transform.position = data.position;
            healthPack.origPos = data.originalPosition; 
            healthPack.transform.eulerAngles = data.eulerAngles;
            healthPack.destination = data.destination;
            healthPack.timePassed = data.timePassed;
            healthPack.name = "HealthPack";
         

 
    }


}
