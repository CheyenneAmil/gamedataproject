﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class GameSaveManager : MonoBehaviour {

    public string gameDataFilename = "game-save.json";
    private string gameDataFilePath;
    public List<string> saveItems;
    public bool firstPlay = true;

    // Singleton pattern from:
    // http://clearcutgames.net/home/?p=437

    // Static singleton property
    public static GameSaveManager Instance { get; private set; }



    //  to account for deprecated OnLevelWasLoaded() - http://answers.unity3d.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
    void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    void Awake() {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this) {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
            return;
        }


        // Here we save our singleton instance
        Instance = this;

        // Furthermore we make sure that we don't destroy between scenes (this is optional)
        DontDestroyOnLoad(gameObject);

        // Folder for gameDataFile
        gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;

        //creates a list of strings to assign as saved items.
        saveItems = new List<string>();
    }


    //gets passed a string from another class and stores it in saveItems.
    public void AddObject(string item) {
        saveItems.Add(item);
    }



    public void Save() {

        //clears all data from the saveItems List
        saveItems.Clear();

        //collects all objects of the type Save under 1 array
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];

        //adds the every item in the array saveableObjects to the list saveItems
        foreach (Save saveableObject in saveableObjects) {
            saveItems.Add(saveableObject.Serialize());
        }

        // writes a new save file populated by data from the saveItems List 
        using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath)) {
            foreach (string item in saveItems) {
                gameDataFileStream.WriteLine(item);
            }
        }
    }

    public void Load() {

        // if the game is loaded, sets firstPlay to false and reloads the current scene
        firstPlay = false;
        saveItems.Clear();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }






    //if its the first play, does nothing, otherwise calls 3 methods
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        if (firstPlay) return;

        LoadSaveGameData();
        DestroyAllSaveableObjectsInScene();
        CreateGameObjects();
    }

    // first, writes the gameSave Data file to saveItems
    void LoadSaveGameData() {
        using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath)) {

            // while the next character in the stream is not nothing
            while (gameDataFileStream.Peek() >= 0) {
                
                string line = gameDataFileStream.ReadLine().Trim();

                //if the line is not empty , add to saveItems
                if (line.Length > 0) {
                    saveItems.Add(line);
                }
            }
        }
    }

    //destroys all objects in scene that are of type Save
    void DestroyAllSaveableObjectsInScene() {
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {
            Destroy(saveableObject.gameObject);
        }
    }


    //reads SaveItems for every item in it and reacreates the saved objects in scene based on the parameters in their own save scripts using the Deserialize method
    void CreateGameObjects() {
        foreach (string saveItem in saveItems) {
            string pattern = @"""prefabName"":""";
            int patternIndex = saveItem.IndexOf(pattern);
            int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
            int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);
            string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);

            GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;
            item.SendMessage("Deserialize", saveItem);
        }
    }
}
