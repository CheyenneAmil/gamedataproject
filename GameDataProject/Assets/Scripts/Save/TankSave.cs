﻿using System;
using UnityEngine;

[Serializable]
public class TankSave : Save {

    public Data data;
    private Tank tank;
    private string jsonString;

    //creates an instance of an abstract subclass called data of type Data which stores 3 transform values
    [Serializable]
    public class Data : BaseData {
        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 destination;
    }

    //gets Tank script component and creates new data value
    void Awake() {
        tank = GetComponent<Tank>();
        data = new Data();
    }

    // returns a string of Json data composed of the prefab name , position, rotation and destination
    public override string Serialize() {
        data.prefabName = prefabName;
        data.position = tank.transform.position;
        data.eulerAngles = tank.transform.eulerAngles;
        data.destination = tank.destination;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    //reads Json data to determine position, angle and destination and feeds its to the gameobject to update itself.
    public override void Deserialize(string jsonData) {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        tank.transform.position = data.position;
        tank.transform.eulerAngles = data.eulerAngles;
        tank.destination = data.destination;
        tank.name = "Tank";
    }
}