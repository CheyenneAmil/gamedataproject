﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using UnityEditor;
using System.Xml;


public class LevelManager : MonoBehaviour {
   
    //Parent to hold all ground gameObjectes 
    public Transform groundHolder;
    //Parent to hold all turret gameObjects
    public Transform turretHolder;
    //Parent to hold all healthPack gameObjects
    public Transform healthPackHolder;

    public enum MapDataFormat {
        Base64,
        CSV
    }

    public MapDataFormat mapDataFormat;
    public Texture2D spriteSheetTexture;

    public GameObject tilePrefab;
    public GameObject turretPrefab;
    public GameObject healthPrefab;
    public List<Sprite> mapSprites;
    public List<GameObject> tiles;
    public List<Vector3> turretPositions;
    public List<Vector3> healthPositions;

    Vector3 tileCenterOffset;
    Vector3 mapCenterOffset;

    public string TMXFilename;

    string gameDirectory;
    string dataDirectory;
    string mapsDirectory;
    string spriteSheetFile;
    string TMXFile;

    public int pixelsPerUnit = 32;

    public int tileWidthInPixels;
    public int tileHeightInPixels;
    float tileWidth;
    float tileHeight;

    public int spriteSheetColumns;
    public int spriteSheetRows;

    public int mapColumns;
    public int mapRows;

    public string mapDataString;
    public List<int> mapData;




    // from http://answers.unity3d.com/questions/10580/editor-script-how-to-clear-the-console-output-wind.html
    static void ClearEditorConsole() {
        Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
        Type type = assembly.GetType("UnityEditor.LogEntries");
        MethodInfo method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }



    static void DestroyChildren(Transform parent) {
        //for every child of the Transform "parent", destroy that child
        for (int i = parent.childCount - 1; i >= 0; i--) {
            DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }



    // NOTE: CURRENTLY ONLY WORKS WITH A SINGLE TILED TILESET

    public void LoadLevel() {

        //clears the editor console log of info
        ClearEditorConsole();
        //destroy all ground gameObjects
        DestroyChildren(groundHolder);
        //destroy all turrets
        DestroyChildren(turretHolder);
        //destroy all healthPacks
        DestroyChildren(healthPackHolder);

    

        //mapsDirectory uses Path.Combine to create a string thats read as the navigation to the Maps folder inside the Streaming Assets folder in assets
        //the TMXFile string takes mapDirectory and links the TMXFilename with it. should read as .../StreamingAssets/Maps/desert-map.tmx
        //TMXfilename should be assigned in the inspector
        {
            mapsDirectory = Path.Combine(Application.streamingAssetsPath, "Maps");
            TMXFile = Path.Combine(mapsDirectory, TMXFilename);
        }


        // clears the map data list which assigns each tile a sprite based on a numeric value which represents that sprite image (ex 29 = sand tile, 0 = left corner brick tile, 1 = top brick tile)
        {
            mapData.Clear();

            //converts the TMX file into a readable string
            string content = File.ReadAllText(TMXFile);


            using (XmlReader reader = XmlReader.Create(new StringReader(content))) {

                // skip to part of file that says map
                reader.ReadToFollowing("map");

                // get the value for width and height of map
                mapColumns = Convert.ToInt32(reader.GetAttribute("width"));
                mapRows = Convert.ToInt32(reader.GetAttribute("height"));

                //skip to part that says tileset
                reader.ReadToFollowing("tileset");

                // get the value tile width and height value
                tileWidthInPixels = Convert.ToInt32(reader.GetAttribute("tilewidth"));
                tileHeightInPixels = Convert.ToInt32(reader.GetAttribute("tileheight"));

                // gets number of different kinds of tiles in tileset
                int spriteSheetTileCount = Convert.ToInt32(reader.GetAttribute("tilecount"));

                //gets number of columns in  tileset and gets rows from this information as well
                spriteSheetColumns = Convert.ToInt32(reader.GetAttribute("columns"));
                spriteSheetRows = spriteSheetTileCount / spriteSheetColumns;

                // skip to part that says image
                reader.ReadToFollowing("image");

                // assigns spritesheetfile the full directory + filename,(after "source"),
                spriteSheetFile = Path.Combine(mapsDirectory, reader.GetAttribute("source"));

                //skip to part that says layer
                reader.ReadToFollowing("layer");

                // skip to part that says data
                reader.ReadToFollowing("data");

                //assogms encodingType to whatever encoding equals
                string encodingType = reader.GetAttribute("encoding");

                //determines what the mapdataformat is based on what the encoding type is
                switch (encodingType) {
                    case "base64":
                        mapDataFormat = MapDataFormat.Base64;
                        break;
                    case "csv":
                        mapDataFormat = MapDataFormat.CSV;
                        break;
                }

                //assigns current element in content and returns it to mapDataString after removing any spaces
                mapDataString = reader.ReadElementContentAsString().Trim();

                //empties turretPositions list
                turretPositions.Clear();
                healthPositions.Clear();


                //***************************************************************
                //       TURRETS                 
                //***************************************************************

                //skips to object group
                if (reader.ReadToFollowing("objectgroup")) {

            

                        //if the next element is of type object
                        if (reader.ReadToDescendant("object"))
                        {


                            //gets the x and y position of the turret and adds a turret under that position
                            do
                            {
                                float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                                float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                                turretPositions.Add(new Vector3(x, -y, 0));

                            } while (reader.ReadToNextSibling("object"));
                        }
                    
                }

                //***************************************************************
                //       HEALTH              
                //***************************************************************

                if (reader.ReadToFollowing("objectgroup"))
                {



                        //if the next element is of type object
                        if (reader.ReadToDescendant("object"))
                        {


                            //gets the x and y position of the turret and adds a turret under that position
                            do
                            {
                                float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                                float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                                healthPositions.Add(new Vector3(x, -y, 0));

                            } while (reader.ReadToNextSibling("object"));
                        }
                    
                }

            }

            // Determines whether the map data format is in Base64 or CSV formats. Identifies the tile type in tile ID and populates the map data list.
            switch (mapDataFormat) {

                case MapDataFormat.Base64:

                    byte[] bytes = Convert.FromBase64String(mapDataString);
                    int index = 0;
                    while (index < bytes.Length) {
                        int tileID = BitConverter.ToInt32(bytes, index) - 1;
                        mapData.Add(tileID);
                        index += 4;
                    }
                    break;


                case MapDataFormat.CSV:

                    string[] lines = mapDataString.Split(new string[] { " " }, StringSplitOptions.None);
                    foreach (string line in lines) {
                        string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);
                        foreach (string value in values) {
                            int tileID = Convert.ToInt32(value) - 1;
                            mapData.Add(tileID);
                        }
                    }
                    break;

            }

        }

        //converts height and width in pixels to usable float for transform values. offsets create correct centers for tiles and map
        {

            tileWidth = (tileWidthInPixels / (float)pixelsPerUnit);
            tileHeight = (tileHeightInPixels / (float)pixelsPerUnit);

            tileCenterOffset = new Vector3(.5f * tileWidth, -.5f * tileHeight, 0);
            mapCenterOffset = new Vector3(-(mapColumns * tileWidth) * .5f, (mapRows * tileHeight) * .5f, 0);

        }




        // creates a new texture and loads the spritesheet onto it. 
        {
            spriteSheetTexture = new Texture2D(2, 2);
            spriteSheetTexture.LoadImage(File.ReadAllBytes(spriteSheetFile));
            spriteSheetTexture.filterMode = FilterMode.Point;
            spriteSheetTexture.wrapMode = TextureWrapMode.Clamp;
        }


        // clears the map of sprites and then creates new sprites based by location (column by row) of the spritesheet. adds them to mapSprites
        {
            mapSprites.Clear();

            for (int y = spriteSheetRows - 1; y >= 0; y--) {
                for (int x = 0; x < spriteSheetColumns; x++) {
                    Sprite newSprite = Sprite.Create(spriteSheetTexture, new Rect(x * tileWidthInPixels, y * tileHeightInPixels, tileWidthInPixels, tileHeightInPixels), new Vector2(0.5f, 0.5f), pixelsPerUnit);
                    mapSprites.Add(newSprite);
                }
            }
        }

        // clears tiles and then populates the board with the tiles  with correct sprites based on their tileID. parents the tiles to ground holder and then adds the tile to a list
        {
            tiles.Clear();

            for (int y = 0; y < mapRows; y++) {
                for (int x = 0; x < mapColumns; x++) {

                    int mapDatatIndex = x + (y * mapColumns);
                    int tileID = mapData[mapDatatIndex];

                    GameObject tile = Instantiate(tilePrefab, new Vector3(x * tileWidth, -y * tileHeight, 0) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;
                    tile.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = mapSprites[tileID];
                    tile.transform.parent = groundHolder;
                    tiles.Add(tile);
                }
            }
        }


        //  creates turrets on scene 
        {
            foreach (Vector3 turretPosition in turretPositions) {
                GameObject turret = Instantiate(turretPrefab, turretPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                turret.name = "Turret";
                turret.transform.parent = turretHolder;
            }
        }

        // creates healthpacks on scene

        {
            foreach(Vector3 healthPos in healthPositions)
            {
                GameObject healthPack = Instantiate(healthPrefab, healthPos + mapCenterOffset, Quaternion.identity) as GameObject;
                healthPack.name = "HealthPack";
                healthPack.transform.parent = healthPackHolder;
            }


        }


        DateTime localDate = DateTime.Now;
        print("Level loaded at: " + localDate.Hour + ":" + localDate.Minute + ":" + localDate.Second);
    }
}


